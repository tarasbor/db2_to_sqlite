#include <iostream>
#include <fstream>
#include <base_db2.hpp>
#include <eq_classes.hpp>
#include <ut_xml_par.hpp>
#include <file_utils.hpp>
#include <sqlite3.h>

#include <error.hpp>
#include <unistd.h> // required fot unlink()

using namespace std;

_db *db=NULL;
_transaction *tr=NULL;

sqlite3 *db3;


int oid=0;
const string table_name="events";

_time event_time;

void init_sqlite3(const string &fname)
{
    int kz=sqlite3_initialize();
    if (kz!=SQLITE_OK)
    {
        throw _error(kz, sqlite3_errstr(kz));
    }
    string bd_fname=fname;

    kz=sqlite3_open_v2(
                bd_fname.c_str(),                                       /* Database filename (UTF-8) */
                &db3,                                                    /* OUT: SQLite db handle */
                SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE,             /* Flags */
                NULL                                                    /* Name of VFS module to use */
                );
    if ( kz != SQLITE_OK)
    {
        sqlite3_close( db3 );
        throw _error(kz, sqlite3_errmsg(db3));
    }

    char *err_pragma;
    kz=sqlite3_exec(db3,"PRAGMA synchronous=off;", 0 ,0, &err_pragma);
    if ( kz != SQLITE_OK)
    {
        sqlite3_close( db3 );
        throw _error(kz, sqlite3_errmsg(db3));
    }
    /* create a statement from an SQL string */
    sqlite3_stmt *stmt = NULL;
    string sql_str= "CREATE  TABLE  IF NOT EXISTS \"main\".\""+table_name+"\" \
            (\"stamp\" BIGINT PRIMARY KEY  NOT NULL UNIQUE,\
              \"event\" VARCHAR(64) NOT NULL ,\
              \"text\" VARCHAR NOT NULL)";

            kz=sqlite3_prepare_v2( db3, sql_str.c_str(), -1, &stmt, NULL );
    if ( kz != SQLITE_OK)
    {
        sqlite3_close( db3 );
        throw _error(kz, sqlite3_errmsg(db3));
    }

    sqlite3_step( stmt );
    if ( kz != SQLITE_OK)
    {
        sqlite3_close( db3 );
        throw _error(kz, sqlite3_errmsg(db3));
    }
    return ;
}


//------------------------------
void save_event(const _time &datetime, const string& event, const string& text )
{
    sqlite3_stmt *stmt = NULL;
    int kz;
    string sql_str=
            "INSERT INTO \"main\".\""+table_name+"\" (\"stamp\",\"event\",\"text\") VALUES (?1,?2,?3)";

    kz=sqlite3_prepare_v2( db3, sql_str.c_str(), -1, &stmt, NULL );
    if ( kz != SQLITE_OK)
    {
        sqlite3_close( db3 );
        throw _error(kz, sqlite3_errmsg(db3));
    }

    kz=sqlite3_bind_int64(stmt, 1,  datetime.stamp());
    if ( kz != SQLITE_OK)
    {
        sqlite3_close( db3 );
        throw _error(kz, sqlite3_errmsg(db3));
    }

    string db_event=event;

    kz=sqlite3_bind_text(stmt, 2, db_event.data(), db_event.length(), NULL);
    if ( kz != SQLITE_OK)
    {
        sqlite3_close( db3 );
        throw _error(kz, sqlite3_errmsg(db3));
    }

    string db_text=text;

    kz=sqlite3_bind_text(stmt, 3, db_text.data(), db_text.length(), NULL);
    if ( kz != SQLITE_OK)
    {
        sqlite3_close( db3 );
        throw _error(kz, sqlite3_errmsg(db3));
    }

    struct timespec tw = {0,1000000};
    struct timespec tr;
    int count_err=0;

    kz=sqlite3_step( stmt );
    while((kz != SQLITE_DONE) && (++count_err < 1000)) // SQLITE_ROW will not be returned in this situation(query).
    {
        // process possible errors
        // First, process need wait events
        if( kz==SQLITE_LOCKED || kz==SQLITE_BUSY )
        {
        }
        else if( kz>0 && kz<100 )
        {
            char tbuff[64];
            sprintf(tbuff,"SQLITE result code:%i",kz);
            sqlite3_close( db3 );
            cout << "ARC:error save_event:" << tbuff << ". Event will be ignored!" << endl;
            return;
            //throw _error(kz, tbuff);
        }
        else {} // if another return code, for future

        kz=sqlite3_step( stmt );
        nanosleep (&tw, &tr);
    };

    sqlite3_reset(stmt);
}

//------------------------------
void make_prior_events()
{
    string info="...Prior..." ;
    cout << info << endl;
    cout << info << "\r"<< flush;

    _statement *st=tr->statement();

    st->prepare("select p.id, p.name, p.vname, p.val, p.prefix, p.wtime_ms from eq.qs_priors p");
    st->execute();

    int cnt=0;

    while(st->fetch())
    {
        if (cnt%10==0) cout << info << cnt << "\r"<< flush;

        bigint db_d;
        st->pop(&db_d);

        string name;
        st->pop(name);

        string vname;
        st->pop(vname);

        int val;
        st->pop(&val);

        string prefix;
        st->pop(prefix);

        int wtime_ms;
        st->pop(&wtime_ms);

        ut_xml_par *par_u=create_ut_xml_parser();
        ut_xml_tag *root_u=par_u->get_root_tag();
        ut_xml_tag *event_u=root_u->add("eq_event","");

        event_u->add("stamp",to_string(event_time.stamp()));

        _eq::_id g_id(oid,db_d);

        char tmp_s[128];
        string event_type="prior.create";
        event_u->add("event",event_type);
        event_u->add("prior_id",g_id.to_c_string(tmp_s));
        event_u->add("name",name);
        event_u->add("vname",vname);
        event_u->add("prefix",prefix);
        event_u->add("val",to_string(val));
        event_u->add("wtime_ms",to_string(wtime_ms));

        string xml_text;
        par_u->to_text(xml_text,"\r\n");
        par_u->release();

        save_event(event_time,event_type,xml_text);

        event_time.tick_100ns(1);
        cnt++;
    }

    delete st;

    cout << info << "OK (" << cnt << ")"<< endl;
}

//------------------------------
void make_groups_events()
{
    string info="...Groups..." ;
    cout << info << endl;
    cout << info << "\r"<< flush;

    _statement *st=tr->statement();

    st->prepare("select g.id, g.name from eq.gl_groups g");
    st->execute();

    int cnt=0;

    while(st->fetch())
    {
        if (cnt%10==0) cout << info << cnt << "\r"<< flush;

        bigint db_g_id;
        st->pop(&db_g_id);

        string g_name;
        st->pop(g_name);

        ut_xml_par *par_u=create_ut_xml_parser();
        ut_xml_tag *root_u=par_u->get_root_tag();
        ut_xml_tag *event_u=root_u->add("eq_event","");

        event_u->add("stamp",to_string(event_time.stamp()));

        _eq::_id g_id(oid,db_g_id);

        char tmp_s[128];
        string event_type="user_group.create";
        event_u->add("event",event_type);
        event_u->add("user_group_id",g_id.to_c_string(tmp_s));
        event_u->add("name",g_name);

        string xml_text;
        par_u->to_text(xml_text,"\r\n");
        par_u->release();

        save_event(event_time,event_type,xml_text);

        event_time.tick_100ns(1);
        cnt++;
    }

    delete st;

    cout << info << "OK (" << cnt << ")"<< endl;
}

//------------------------------
void make_opers_events()
{
    string info="...Opers..." ;
    cout << info << endl;
    cout << info << "\r"<< flush;

    _statement *st=tr->statement();

    st->prepare("select o.id, o.name, u.id, u.login, o.is_hide from eq.gl_users u, eq.opers o where o.user_id=u.id");
    st->execute();

    int cnt=0;

    while(st->fetch())
    {
        if (cnt%10==0) cout << info << cnt << "\r"<< flush;

        bigint db_o_id;
        st->pop(&db_o_id);

        string o_name;
        st->pop(o_name);

        bigint db_u_id;
        st->pop(&db_u_id);

        string u_login;
        st->pop(u_login);

        int is_hide;
        st->pop(&is_hide);

        list<string> l_groups;
        string in_groups;

        _statement *st_gr=tr->statement();
//        select group_id from eq.gl_users_groups where user_id
        st_gr->prepare("select group_id from eq.gl_users_groups where user_id=:id<bigint>");
        st_gr->push(db_u_id);
        st_gr->execute();
        while(st_gr->fetch())
        {
            _db_id gid;
            st_gr->pop(gid);

            char id_str[32];

            l_groups.push_back(gid.value_str(id_str));
        }
        delete st_gr;

        list<string>::iterator it=l_groups.begin();
        while(it!=l_groups.end())
        {
            in_groups+=*it;
            ++it;
            if (it!=l_groups.end())
            {
                in_groups+=", ";
            }
        }
        ut_xml_par *par_u=create_ut_xml_parser();
        ut_xml_tag *root_u=par_u->get_root_tag();
        ut_xml_tag *event_u=root_u->add("eq_event","");

        event_u->add("stamp",to_string(event_time.stamp()));

        _eq::_id u_id(oid,db_u_id);

        char tmp_s[128];
        string event_type="user.create";
        event_u->add("event",event_type);
        event_u->add("user_id",u_id.to_c_string(tmp_s));
        event_u->add("login",u_login);
        event_u->add("name",o_name);
        event_u->add("group_ids",in_groups);

        string xml_text;
        par_u->to_text(xml_text,"\r\n");
        par_u->release();

        save_event(event_time,event_type,xml_text);

        ut_xml_par *par_o=create_ut_xml_parser();
        ut_xml_tag *root_o=par_o->get_root_tag();
        ut_xml_tag *event_o=root_o->add("eq_event","");
        _eq::_id o_id(oid,db_o_id);

        event_time.tick_100ns(1);

        event_o->add("stamp",to_string(event_time.stamp()));

        event_type="oper.create";
        event_o->add("event",event_type);
        event_o->add("oper_id",o_id.to_c_string(tmp_s));
        event_o->add("user_id",u_id.to_c_string(tmp_s));
        event_o->add("name",o_name);
        event_o->add("hidden",to_string(is_hide));

        xml_text.clear();
        par_o->to_text(xml_text,"\r\n");
        par_o->release();

        save_event(event_time,event_type,xml_text);

        event_time.tick_100ns(1);
        cnt++;
    }

    delete st;

    cout << info << "OK (" << cnt << ")"<< endl;
}

//------------------------------
void make_wps_events()
{
    string info="...WPs..." ;
    cout << info << endl;
    cout << info << "\r"<< flush;

    _statement *st=tr->statement();

    st->prepare("select id, name, place from eq.wplaces");
    st->execute();

    int cnt=0;
    while(st->fetch())
    {
        if (cnt%10==0) cout << info << cnt << "\r"<< flush;
        bigint db_id;
        st->pop(&db_id);

        string wp_name;
        st->pop(wp_name);

        string wp_place;
        st->pop(wp_place);

        ut_xml_par *par=create_ut_xml_parser();
        ut_xml_tag *root=par->get_root_tag();
        ut_xml_tag *event=root->add("eq_event","");

        event->add("stamp",to_string(event_time.stamp()));

        _eq::_id wp_id(oid,db_id);

        char tmp_s[128];

        string event_type="wplace.create";
        event->add("event",event_type);
        event->add("wplace_id",wp_id.to_c_string(tmp_s));
        event->add("name",wp_name);
        event->add("place",wp_place);

        _statement *st2=tr->statement();
        st2->prepare("select q_id from eq.qs_wplaces_map where wplace_id=:id<bigint>");
        st2->push(db_id);
        st2->execute();

        string qids_st;

        while(st2->fetch())
        {
            _db_id q_db_id;
            st2->pop(q_db_id);

            if (!qids_st.empty()) qids_st.append(1,',');
            qids_st+=q_db_id.value_str(tmp_s);
        }
        delete st2;

        event->add("q_ids",qids_st);

        string xml_text;
        par->to_text(xml_text,"\r\n");
        par->release();

        save_event(event_time,event_type,xml_text);

        event_time.tick_100ns(1);
        cnt++;
    }

    delete st;

    cout << info << "OK (" << cnt << ")"<< endl;
}

//------------------------------
void make_qs_events()
{
    string info="...Qs..." ;
    cout << info << endl;
    cout << info << "\r"<< flush;

    _statement *st=tr->statement();

    st->prepare("select id, name, vname, prefix, snumber from eq.qs");
    st->execute();

    int cnt=0;
    while(st->fetch())
    {
        if (cnt%10==0) cout << info << cnt << "\r"<< flush;
        bigint db_id;
        st->pop(&db_id);

        string q_name;
        st->pop(q_name);

        string q_vname;
        st->pop(q_vname);

        string q_prefix;
        st->pop(q_prefix);

        int snumber;
        st->pop(&snumber);

        ut_xml_par *par=create_ut_xml_parser();
        ut_xml_tag *root=par->get_root_tag();
        ut_xml_tag *event=root->add("eq_event","");

        event->add("stamp",to_string(event_time.stamp()));

        _eq::_id q_id(oid,db_id);

        char tmp_s[128];

        string event_type="q.create";
        event->add("event",event_type);
        event->add("q_id",q_id.to_c_string(tmp_s));
        event->add("name",q_name);
        event->add("vname",q_vname);
        event->add("prefix",q_prefix);
        event->add("snumber",to_string(snumber));

        string xml_text;
        par->to_text(xml_text,"\r\n");
        par->release();

        save_event(event_time,event_type,xml_text);

        event_time.tick_100ns(1);
        cnt++;
    }

    delete st;

    cout << info << "OK (" << cnt << ")"<< endl;
}

//------------------------------
void make_tickets_events()
{
    string info="...Tickets..." ;
    cout << info << endl;
    cout << info << "\r"<< flush;

    _statement *st=tr->statement();

    st->prepare("select id, number, issue_number, create_ftime, proc_code, start_proc_time, end_proc_time, put_to_q_time, precord_time, oper_id, wplace_id, q_id, prior_id, mark from eq.htickets order by end_proc_time");// limit 100");
    st->execute();

    int cnt=0;
    while(st->fetch())
    {
        if (cnt%100==0) cout << info << cnt << "\r"<< flush;

        _db_id db_id;
        st->pop(db_id);

        string number;
        st->pop(number);

        int issue_number;
        st->pop(&issue_number);

        _time create_time;
        st->pop(create_time);

        int proc_code;
        st->pop(&proc_code);

        _time start_proc_time;
        st->pop(start_proc_time);

        _time end_proc_time;
        st->pop(end_proc_time);

        _time stamp_time=end_proc_time;

        _time put_to_q_time;
        st->pop(put_to_q_time);

        _time precord_time;
        st->pop(precord_time);

        _db_id oper_db_id;
        st->pop(oper_db_id);

        _db_id wplace_db_id;
        st->pop(wplace_db_id);

        _db_id q_db_id;
        st->pop(q_db_id);

        _db_id db_prior_id;
        st->pop(db_prior_id);

        int mark;
        st->pop(&mark);

        ut_xml_par *par=create_ut_xml_parser();
        ut_xml_tag *root=par->get_root_tag();
        ut_xml_tag *event=root->add("eq_event","");

        event->add("stamp",to_string(stamp_time.stamp()));

        _eq::_id t_id(oid,db_id.value());
        _eq::_id wp_id(oid,wplace_db_id.value());
        _eq::_id o_id(oid,oper_db_id.value());
        _eq::_id q_id(oid,q_db_id.value());
        _eq::_id prior_id(oid,db_prior_id.value());

        char tmp_s[128];

        string event_type="wplace.close_proc_ticket";
        event->add("event",event_type);
        event->add("ticket_id",t_id.to_c_string(tmp_s));
        event->add("wplace_id",wp_id.to_c_string(tmp_s));
        event->add("oper_id",o_id.to_c_string(tmp_s)); // may be NULL
        event->add("q_id",q_id.to_c_string(tmp_s));
        event->add("prior_id",prior_id.to_c_string(tmp_s));

        event->add("proc_code",to_string(proc_code));

        event->add("issue_number", to_string(issue_number));   // remembe - we get it from DB - in db one record - one issue

        event->add("mark",to_string(mark));

        event->add("number",number);

        event->add("create_ftime",to_string(create_time.stamp()));
        event->add("put_to_q_time",to_string(put_to_q_time.stamp()));
        event->add("precord_time",to_string(precord_time.stamp()));
        event->add("start_proc_time",to_string(start_proc_time.stamp()));
        event->add("end_proc_time",to_string(end_proc_time.stamp()));

        string xml_text;
        par->to_text(xml_text,"\r\n");
        par->release();

        save_event(stamp_time,event_type,xml_text);
        cnt++;
    }

    delete st;

    cout << info << "OK (" << cnt << ")"<< endl;
}

//------------------------------
void make_opers_timeline_events()
{
    string info="...Opers timeline..." ;
    cout << info << endl;
    cout << info << "\r"<< flush;

    _statement *st=tr->statement();

    st->prepare("select oper_id, event_stamp, event_code, ref_id from eq.oper_timeline  order by event_stamp");// limit 100");
    st->execute();

    int cnt=0;
    while(st->fetch())
    {
        if (cnt%100==0) cout << info << cnt << "\r"<< flush;

        _db_id oper_db_id;
        st->pop(oper_db_id);

        _time event_stamp;
        st->pop(event_stamp);

        int event_code_i;
        st->pop(&event_code_i);

        _db_id wplace_db_id;
        st->pop(wplace_db_id);

        ut_xml_par *par=create_ut_xml_parser();
        ut_xml_tag *root=par->get_root_tag();
        ut_xml_tag *event=root->add("eq_event","");

        //char stamp_s[128];
        //event_stamp.stamp_to_str(stamp_s);

        _eq::_id wp_id(oid,wplace_db_id.value());
        _eq::_id o_id(oid,oper_db_id.value());

        char tmp_s[128];
        string event_type;

        if      (event_code_i==_eq::_wplace::wait) event_type="wplace.reg_oper";
        else if (event_code_i==_eq::_wplace::offline) event_type="wplace.unreg_oper";
        else if (event_code_i==_eq::_wplace::pause) event_type="wplace.pause_work";
        else if (event_code_i==_eq::_wplace::proc) event_type="wplace.start_proc_ticket";
        event->add("event",event_type);

        event->add("event_code",to_string(event_code_i));

        event->add("wplace_id",wp_id.to_c_string(tmp_s));
        event->add("oper_id",o_id.to_c_string(tmp_s));

        event->add("stamp",to_string(event_stamp.stamp()));

        string xml_text;
        par->to_text(xml_text,"\r\n");
        par->release();

        save_event(event_stamp,event_type,xml_text);
        cnt++;
    }

    delete st;

    cout << info << "OK (" << cnt << ")"<< endl;
}


//------------------------------
int main(int n,char *p[])
{
    cout << "VER:2.01" << endl;
    if (n<5)
    {
        cout << "Usage: " << p[0] << " db_dsn db_user db_pass out_file" << endl;
        return -1;
    }

    string db_dsn=p[1];
    string db_user=p[2];
    string db_pass=p[3];
    string out_file=p[4];

    cout << endl;
    cout << "Make events from " << db_dsn << " database to " << out_file << endl;

    cout << "Begin:" << endl;

    cout << "Check " << out_file;
    if (fu_is_file_exist(out_file))
    {
        cout << " - exist...remove it first...";
        unlink(out_file.c_str());
        unlink((out_file+"-journal").c_str());
        cout << "and will created." << endl;
    }
    else
    {
        cout << " - not exist...will created." << endl;
    }

    cout << "Connect to " << db_dsn << endl;

    event_time=_time(1970,1,1,0,0,0,0);


    try
    {
        db=create_db_db2();
        db->connect(db_dsn,db_user,db_pass);
        tr=db->transaction();
        tr->start();
        _statement *st=tr->statement();  // In case of exception no delete. But program too simple don`t worry.
                                        // It will deleted when terminate.
        init_sqlite3(out_file);
        _time tStart;
        tStart.set_now();
        cout << "Connected to " << db_dsn << endl;

        // find office_id
        string  officeId;
        st->prepare("select cvalue from eq.constants where name='office_id'");
        st->execute();
        if (st->fetch())
        {
            st->pop(officeId);
            oid=atoi(officeId.c_str());
        }
        else
            throw _error(1001,"DB content error. No column NAME=`office_id` found in eq.constants");
        cout << "Office id: " << officeId << endl;


        make_groups_events();
        make_opers_events();

        make_wps_events();
        make_qs_events();
        make_prior_events();

        make_tickets_events();
        make_opers_timeline_events();

        _time tStop;
        tStop.set_now();
        cout << "time start: " << tStart.stamp_to_str().c_str() << endl;
        cout << "time stop : " << tStop.stamp_to_str().c_str() << endl;
        cout << "          : " << tStop.diff_ms(tStart) << "[ms]" << endl;

        sqlite3_close( db3 );
        sqlite3_shutdown();

        st->close();
        delete st;

        tr->commit();
        delete tr;

        db->disconnect();
        delete db;
    }
    catch(_db_error &e)
    {
        if(tr) {tr->commit(); delete tr; }
        if(db) {db->disconnect(); delete db; }
        cout << "DB error: " << e.error_st() << endl;
        sqlite3_close( db3 );
        sqlite3_shutdown();
        return -2;
    }
    catch(_error &e)
    {
        if(tr) {tr->commit(); delete tr; }
        if(db) {db->disconnect(); delete db; }
        cout << "SQLite error: " << e.str() << endl;
        sqlite3_close( db3 );
        sqlite3_shutdown();
        return -3;
    }

    cout << "All done." << endl;

    return 0;
}
