#-------------------------------------------------
#
# Project created by QtCreator 2014-10-31T18:40:28
#
#-------------------------------------------------

TEMPLATE = app
#CONFIG += console
#CONFIG += windows
CONFIG -= app_bundle
CONFIG -= qt

TARGET = db2_to_sqlite

QMAKE_CXXFLAGS += -std=c++0x

#CONFIG(windows) {
#    DEFINES *= WINDOWS
#}
DEFINES -= UNICODE
DEFINES *= DB_USE_COMMON_TIME
DEFINES *= TCP_SERVER_TO_REINIT=3600000
DEFINES *= KEEP_ALIVE_TIME=0

CONFIG(debug, debug|release) {
#    DEFINES += NET_LIB_LOG
    DEFINES += AS_APP
    DEFINES -= AS_SERVICE
    message("--- debug ---")
}
else {
    DEFINES -= AS_APP
    DEFINES += AS_SERVICE
    message("--- release ---")
}

INCLUDEPATH += ../../../libcommon
INCLUDEPATH += ../../../libcommon/system
INCLUDEPATH += ../../../libcommon/log2
INCLUDEPATH += ../../../libcommon/thread
INCLUDEPATH += ../../../libcommon/net
INCLUDEPATH += ../../../libcommon/otl4
INCLUDEPATH += ../../../libcommon/obj_db5
INCLUDEPATH += ../../../libcommon/xml
INCLUDEPATH += ..././../libcommon/xml/lib
INCLUDEPATH += ../../../libcommon/htmlreport
INCLUDEPATH += ../../eq_common
INCLUDEPATH += /opt/ibm/db2/V9.7/include
INCLUDEPATH +=     ../../../libcommon/sqlite3


#win32:LIBS += C:/Qt/Qt502/Tools/MinGW/i686-w64-mingw32/lib/libws2_32.a
unix:LIBS += /lib/librt.so.1
unix:LIBS += /lib/libpthread.so.0
unix:LIBS += /opt/ibm/db2/V9.7/lib32/libdb2.so
unix:LIBS += /lib/libdl.so.2

SOURCES += \
    main.cpp \
    ../../../libcommon/sqlite3/sqlite3.c \
    ../../../libcommon/obj_db5/base_db2.cpp \
    ../../../libcommon/thread/clthread.cpp \
    ../../../libcommon/system/time.cpp \
    ../../../libcommon/log2/log.cpp \
    ../../../libcommon/log2/log_file_writer.cpp \
    ../../../libcommon/system/error.cpp \
    ../../../libcommon/system/text_help.cpp \
    ../../../libcommon/system/config_parser.cpp \
    ../../../libcommon/xml/lib/ut_xml_par.cpp \
    ../../../libcommon/xml/lib/ut_xml_tag.cpp \
    ../../../libcommon/system/codepage.cpp \
    ../../../libcommon/system/file_utils.cpp

HEADERS += \
    ../../../libcommon/sqlite3/sqlite3.h \
    ../../../libcommon/obj_db5/base_db2.hpp \
    ../../../libcommon/thread/clthread.hpp \
    ../../../libcommon/system/time.hpp \
    ../../../libcommon/log2/log.hpp \
    ../../../libcommon/log2/log_file_writer.hpp \
    ../../../libcommon/xml/ut_xml_par.hpp \
    ../../../libcommon/xml/ut_xml_tag.hpp \
    ../../../libcommon/system/error.hpp \
    ../../../libcommon/system/text_help.hpp \
    ../../../libcommon/system/config_parser.hpp \
    ../../../libcommon/otl4/otlv4.h \
    ../../../libcommon/system/codepage.hpp \
    ../../../libcommon/system/file_utils.hpp
